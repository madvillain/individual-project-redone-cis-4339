require 'test_helper'

class ItemsBoughtsControllerTest < ActionController::TestCase
  setup do
    @items_bought = items_boughts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:items_boughts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create items_bought" do
    assert_difference('ItemsBought.count') do
      post :create, items_bought: { AmountBought: @items_bought.AmountBought, DateIn: @items_bought.DateIn, ProductID: @items_bought.ProductID }
    end

    assert_redirected_to items_bought_path(assigns(:items_bought))
  end

  test "should show items_bought" do
    get :show, id: @items_bought
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @items_bought
    assert_response :success
  end

  test "should update items_bought" do
    patch :update, id: @items_bought, items_bought: { AmountBought: @items_bought.AmountBought, DateIn: @items_bought.DateIn, ProductID: @items_bought.ProductID }
    assert_redirected_to items_bought_path(assigns(:items_bought))
  end

  test "should destroy items_bought" do
    assert_difference('ItemsBought.count', -1) do
      delete :destroy, id: @items_bought
    end

    assert_redirected_to items_boughts_path
  end
end
