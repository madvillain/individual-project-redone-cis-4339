class CreateItemsSolds < ActiveRecord::Migration
  def change
    create_table :items_solds do |t|
      t.integer :ProductID
      t.integer :amountSold
      t.decimal :Revenue
      t.date :dateOut

      t.timestamps
    end
  end
end
