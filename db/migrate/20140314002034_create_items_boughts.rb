class CreateItemsBoughts < ActiveRecord::Migration
  def change
    create_table :items_boughts do |t|
      t.integer :ProductID
      t.integer :AmountBought
      t.date :DateIn

      t.timestamps
    end
  end
end
