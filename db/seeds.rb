# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Products = Product.create([ {name: 'T-Shirts', price: '10.00', quantityOnHand:'58'},
                            {name: 'Shorts', price: '15.00', quantityOnHand:'53'},
                            {name: 'Jeans', price: '25.00', quantityOnHand:'55'},
                            {name: 'Polo Shirts', price: '20.00', quantityOnHand:'48'},
                            {name: 'Button Ups', price: '30.00', quantityOnHand:'47'},
                            {name: 'Socks', price: '8.00', quantityOnHand:'51'}
                            ])
