class ItemsBoughtsController < ApplicationController
  before_action :set_items_bought, only: [:show, :edit, :update, :destroy]

  # GET /items_boughts
  # GET /items_boughts.json
  def index
    @items_boughts = ItemsBought.all
  end

  # GET /items_boughts/1
  # GET /items_boughts/1.json
  def show
  end

  # GET /items_boughts/new
  def new
    @items_bought = ItemsBought.new
  end

  # GET /items_boughts/1/edit
  def edit
  end

  # POST /items_boughts
  # POST /items_boughts.json
  def create
    @items_bought = ItemsBought.new(items_bought_params)

    respond_to do |format|
      if @items_bought.save
        format.html { redirect_to @items_bought, notice: 'Items bought was successfully created.' }
        format.json { render action: 'show', status: :created, location: @items_bought }
      else
        format.html { render action: 'new' }
        format.json { render json: @items_bought.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items_boughts/1
  # PATCH/PUT /items_boughts/1.json
  def update
    respond_to do |format|
      if @items_bought.update(items_bought_params)
        format.html { redirect_to @items_bought, notice: 'Items bought was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @items_bought.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items_boughts/1
  # DELETE /items_boughts/1.json
  def destroy
    @items_bought.destroy
    respond_to do |format|
      format.html { redirect_to items_boughts_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_items_bought
      @items_bought = ItemsBought.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def items_bought_params
      params.require(:items_bought).permit(:ProductID, :AmountBought, :DateIn)
    end
end
