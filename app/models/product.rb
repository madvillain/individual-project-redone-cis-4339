class Product < ActiveRecord::Base

  has_many :items_boughts
  has_many :items_solds
  validates_presence_of :name, :price
  def total_count
    total_count = 0
    ItemsBought.all.each do |c|
      total_count += c.AmountBought
    end
    total_count
  end

end
