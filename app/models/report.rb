class Report < ActiveRecord::Base

  belongs_to :items_bought
  belongs_to :items_sold
  belongs_to :product




  def total_gross_revenue
    total_count = 0
    ItemsSold.all.each do |r|
      total_count += r.Revenue
    end
    total_count
  end

  def weekly_sold
    total_count = 0
    ItemsSold.all.each do |s|
      total_count += s.amountSold
    end
    total_count/52
  end



end
