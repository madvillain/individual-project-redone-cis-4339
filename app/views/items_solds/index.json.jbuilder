json.array!(@items_solds) do |items_sold|
  json.extract! items_sold, :id, :ProductID, :amountSold, :Revenue, :dateOut
  json.url items_sold_url(items_sold, format: :json)
end
