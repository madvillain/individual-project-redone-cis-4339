json.array!(@items_boughts) do |items_bought|
  json.extract! items_bought, :id, :ProductID, :AmountBought, :DateIn
  json.url items_bought_url(items_bought, format: :json)
end
